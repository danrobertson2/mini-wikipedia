FROM alpine:latest

RUN apk update --no-cache && apk add --virtual build-dependencies \
  wget \
  openssl \
  ca-certificates && \
  mkdir /zim && \
  cd /zim && \
  wget http://download.kiwix.org/zim/wikipedia/wikipedia_en_all_nopic_2018-09.zim && \
  mkdir /kiwix && \
  cd /kiwix && \
  wget https://download.kiwix.org/bin/kiwix-linux-x86_64.tar.bz2 && \
  tar -jxf kiwix-linux-x86_64.tar.bz2 && \
  apk del build-dependencies && \
  apk add bash

ADD scripts/start.sh /start.sh

EXPOSE 80

CMD ["/start.sh"]
