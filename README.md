# mini-wikipedia

## Summary
An english text only wikipedia container

## Building

> git clone https://gitlab.com/danrobertson2/mini-wikipedia.git  
> docker build -t mini-wikipedia:latest .

## Running

To run the container:

> docker run -d -p 80:80 danrobertson2/mini-wikipedia:latest

You can then browse to http://127.0.0.1 to view the offline version of wikipedia. To find your DOCKER_HOST use the docker inspect to get the IP address.

![](./it-works-0.png)
![](./it-works-1.png) 

## Notes

- add option to use different .zim files
